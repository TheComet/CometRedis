package co.thecomet.redis.bungee.db;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "network_proxy_settings", noClassnameStored = true)
public class ProxySettings {
    @Id
    public ObjectId id;
    public String motd = "";
    public int maxPlayers = 1000;
}
