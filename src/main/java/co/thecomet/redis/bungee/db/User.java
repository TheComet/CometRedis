package co.thecomet.redis.bungee.db;

import co.thecomet.common.user.Rank;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.UUID;

@Entity(value = "network_users", noClassnameStored = true)
public class User {
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public Rank rank;
    
    public User() {}
    
    public User(UUID uuid, Rank rank) {
        this.uuid = uuid.toString();
        this.rank = rank;
    }
}
