package co.thecomet.redis.bungee;

import co.thecomet.redis.redis.RediSettings;
import co.thecomet.redis.utils.JsonConfig;

public class DynamicSettings extends JsonConfig {
    private RediSettings redis = new RediSettings("localhost", 6379, null, -1);

    public RediSettings getRedis() {
        return redis;
    }
}
