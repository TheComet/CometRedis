package co.thecomet.redis.bungee.server;

import co.thecomet.common.user.Rank;
import co.thecomet.redis.bungee.CRBungee;
import co.thecomet.redis.bungee.db.User;
import co.thecomet.redis.redis.pubsub.NetTaskSubscribe;
import net.jodah.expiringmap.ExpiringMap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.HashMap;
import java.util.UUID;

public class MaintenanceReceiver implements Listener {
    public MaintenanceReceiver(CRBungee plugin) {
        plugin.getRedis().registerChannel("maintenance");
        ProxyServer.getInstance().getPluginManager().registerListener(plugin, this);
    }

    @NetTaskSubscribe(args = {"maintenance"}, name = "maintenance")
    public void onMaintenance(HashMap<String, Object> args) {
        Object m = args.get("maintenance");

        if (m == null) {
            return;
        }

        if (!(m instanceof Boolean)) {
            return;
        }

        boolean maintenance = ((Boolean)m).booleanValue();
        CRBungee.setMaintenance(maintenance);

        if (CRBungee.isMaintenance()) {
            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                User user = CRBungee.getUsers().get(player.getUniqueId());
                if (user == null || user.rank.ordinal() < Rank.HELPER.ordinal()) {
                    player.disconnect("Currently under maintenance. We will be back very soon.");
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onLogin(LoginEvent event) {
        ExpiringMap<UUID, User> users = CRBungee.getUsers();
        
        if (event.getConnection() == null) {
            ProxyServer.getInstance().getLogger().info("Connection Null");
            return;
        }

        if (event.getConnection().getUniqueId() == null) {
            ProxyServer.getInstance().getLogger().info("UUID Null");
            return;
        }

        User user = users.get(event.getConnection().getUniqueId());
        if (user == null) {
            user = CRBungee.getUserDAO().findOne(CRBungee.getUserDAO().createQuery().field("uuid").equal(event.getConnection().getUniqueId().toString()));
            users.put(event.getConnection().getUniqueId(), user == null ? new User(event.getConnection().getUniqueId(), Rank.DEFAULT) : user);
        }

        if (CRBungee.isMaintenance()) {
            if (user == null || user.rank == null || user.rank.ordinal() < Rank.HELPER.ordinal()) {
                event.setCancelReason("Currently under maintenance. We will be back very soon.");
                event.setCancelled(true);
            }
        }
    }
}
