package co.thecomet.redis.bungee.server;

import net.jodah.expiringmap.ExpiringMap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import co.thecomet.redis.bungee.CRBungee;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class ServerHeartbeatHandler implements Runnable {
    /**
     * Stores previous heartbeats.
     */
    private ExpiringMap<ServerInfo, Heartbeat> heartbeats = ExpiringMap.builder().expiration(30, TimeUnit.SECONDS).build();

    /**
     * Stores the current task.
     */
    private ScheduledTask task;

    /**
     * Creates a new handler, and schedules it in the BungeeCord scheduler.
     */
    public ServerHeartbeatHandler() {
        schedule();
    }

    /**
     * Call this method when a server sends a heartbeat.
     *
     * @param info The server that a heartbeat was received for.
     */
    public void heartbeatReceived(ServerInfo info, List playerList, boolean vipOnly) {
        ArrayList<String> players = new ArrayList<>();
        for (Object p : playerList) {
            if ((p instanceof String) == false) {
                continue;
            }
            players.add((String) p);
        }
        this.heartbeats.put(info, new Heartbeat(info, Calendar.getInstance().getTimeInMillis(), players, vipOnly));
    }

    @Override
    public void run() {
        ConcurrentMap<String, ServerInfo> allServerInfo = DynamicRegistrationModule.getInstance().getServerInfo();
        for (ServerInfo info : allServerInfo.values()) {
            Heartbeat heartbeat = heartbeats.get(info);
            if (heartbeat == null) {
                ProxyServer.getInstance().getServers().remove(info.getName());
                ServerHandler.disconnectAll(info);
                this.heartbeats.remove(info);
            }
        }
        schedule();
    }

    /**
     * Reschedule this in the scheduler for execution.
     */
    public void schedule() {
        task = ProxyServer.getInstance().getScheduler().schedule(CRBungee.getInstance(), this, 30, TimeUnit.SECONDS);
    }

    /**
     * Get players online
     */
    public static Integer getPlayersOnline() {
        List<ServerInfo> infos = new ArrayList<>(DynamicRegistrationModule.getInstance().serverHeartbeatHandler.heartbeats.keySet());
        Integer count = 0;
        for (ServerInfo info : infos) {
            count += info.getPlayers().size();
        }
        return count;
    }
    
    public static boolean isVipOnly(ServerInfo info) {
        Heartbeat heartbeat = DynamicRegistrationModule.getInstance().serverHeartbeatHandler.heartbeats.get(info);
        if (heartbeat == null) {
            return false;
        }
        
        return heartbeat.isVipOnly();
    }

    public void shutdown() {
        task.cancel();
    }
}
