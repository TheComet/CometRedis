package co.thecomet.redis.bungee.server;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import co.thecomet.redis.bungee.CRBungee;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class DynamicRegistrationModule {
    private static DynamicRegistrationModule instance;
    protected ServerHeartbeatHandler serverHeartbeatHandler;

    public DynamicRegistrationModule(CRBungee plugin) {
        instance = this;
        this.serverHeartbeatHandler = new ServerHeartbeatHandler();
        register(plugin);
    }

    private void register(CRBungee plugin) {
        plugin.getRedis().registerChannel("heartbeat");
        plugin.getRedis().registerTask(new ServerHandler());
        plugin.getRedis().registerTask(new BaseReceiver(plugin));
        plugin.getRedis().registerTask(new MaintenanceReceiver(plugin));
    }

    public ConcurrentMap<String, ServerInfo> getServerInfo() {
        return new ConcurrentHashMap<>(ProxyServer.getInstance().getServers());
    }

    public static DynamicRegistrationModule getInstance() {
        return instance;
    }

    public ServerHeartbeatHandler getServerHeartbeatHandler() {
        return serverHeartbeatHandler;
    }
}
