package co.thecomet.redis.bungee;

import co.thecomet.db.mongodb.ResourceManager;
import co.thecomet.redis.bungee.db.ProxySettings;
import co.thecomet.redis.bungee.db.User;
import co.thecomet.redis.bungee.server.DynamicRegistrationModule;
import co.thecomet.redis.bungee.server.ProxySettingsManager;
import co.thecomet.redis.redis.RedisHandler;
import co.thecomet.redis.utils.JsonConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.jodah.expiringmap.ExpiringMap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CRBungee extends Plugin {
    private static Gson gson = new GsonBuilder().create();
    private static CRBungee instance;
    private static boolean maintenance = false;
    private static ExpiringMap<UUID, User> users = ExpiringMap.builder().expiration(3, TimeUnit.MINUTES).expirationListener(new ExpiringMap.ExpirationListener<UUID, User>() {
        @Override
        public void expired(UUID uuid, User user) {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null) {
                ProxyServer.getInstance().getScheduler().schedule(CRBungee.getInstance(), () -> users.put(uuid, user), 0L, TimeUnit.MILLISECONDS);
            }
        }
    }).build();
    private static Datastore datastore;
    private static BasicDAO<User, ObjectId> userDAO;
    private static BasicDAO<ProxySettings, ObjectId> proxySettingsDAO;

    private DynamicSettings dynamicSettings;
    private RedisHandler<Plugin> redis;
    private DynamicRegistrationModule dynamicRegistrationModule;
    private ProxySettings proxySettings;

    public void onEnable() {
        instance = this;

        initMongoDB();
        loadConfig();
        redis = init("redis.json");
        new ProxySettingsManager(proxySettings);
        dynamicRegistrationModule = new DynamicRegistrationModule(this);
    }

    private RedisHandler init(String file) {
        return new RedisHandler<>(getLogger(), dynamicSettings.getRedis(), this, ProxyServer.getInstance().getScheduler()::runAsync);
    }
    
    public void initMongoDB() {
        datastore = ResourceManager.getInstance().getDatastore(new HashSet<Class>(new ArrayList<Class>(){{
            add(User.class);
        }}));
        datastore.ensureCaps();
        datastore.ensureIndexes();
        userDAO = new BasicDAO<>(User.class, datastore);
        proxySettingsDAO = new BasicDAO<>(ProxySettings.class, datastore);
        
        proxySettings = loadProxySettings();
    }

    public void onDisable() {
        redis.disable();
    }

    private DynamicSettings loadConfig() {
        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        File file = new File(getDataFolder(), "settings.json");
        dynamicSettings = JsonConfig.load(file, DynamicSettings.class);

        if (!file.exists()) {
            dynamicSettings.save(file);
        }

        return dynamicSettings;
    }
    
    private ProxySettings loadProxySettings() {
        ProxySettings settings = proxySettingsDAO.find().get();
        if (settings == null) {
            settings = new ProxySettings();
            proxySettingsDAO.save(settings);
        }
        return settings;
    }

    public RedisHandler getRedis() {
        return redis;
    }

    public DynamicRegistrationModule getDynamicRegistrationModule() {
        return dynamicRegistrationModule;
    }

    public static CRBungee getInstance() {
        return instance;
    }

    public DynamicSettings getDynamicSettings() {
        return dynamicSettings;
    }
    
    public static boolean isMaintenance() {
        return maintenance;
    }
    
    public static void setMaintenance(boolean m) {
        maintenance = m;
    }
    
    public static ExpiringMap<UUID, User> getUsers() {
        return users;
    }
    
    public static BasicDAO<User, ObjectId> getUserDAO() {
        return userDAO;
    }

    public static BasicDAO<ProxySettings, ObjectId> getProxySettingsDAO() {
        return proxySettingsDAO;
    }

    public ProxySettings getProxySettings() {
        return proxySettings;
    }
}
