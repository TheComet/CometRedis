package co.thecomet.redis.bukkit;

import co.thecomet.redis.redis.RediSettings;
import co.thecomet.redis.utils.JsonConfig;

public class DynamicSettings extends JsonConfig {
    private String name = "server1";
    private RediSettings redis = new RediSettings("localhost", 6379, null, -1);

    public String getName() {
        return name;
    }

    public RediSettings getRedis() {
        return redis;
    }
}
