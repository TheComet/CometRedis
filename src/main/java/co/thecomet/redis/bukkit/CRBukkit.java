package co.thecomet.redis.bukkit;

import co.thecomet.redis.redis.pubsub.NetTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import co.thecomet.redis.redis.RedisHandler;
import co.thecomet.redis.utils.JsonConfig;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CRBukkit extends JavaPlugin {
    private static Gson gson = new GsonBuilder().create();
    private static CRBukkit instance;
    private static boolean vipOnly = false;

    private DynamicSettings dynamicSettings;
    private RedisHandler<JavaPlugin> redis;
    private BukkitTask task;

    public void onEnable() {
        instance = this;

        loadConfig();
        redis = init("redis.json");
        register();
    }

    private RedisHandler init(String file) {
        return new RedisHandler<>(getLogger(), dynamicSettings.getRedis(), this, Bukkit.getScheduler()::runTaskAsynchronously);
    }

    public void onDisable() {
        redis.disable();
    }

    private DynamicSettings loadConfig() {
        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        File file = new File(getDataFolder(), "settings.json");
        dynamicSettings = JsonConfig.load(file, DynamicSettings.class);

        if (!file.exists()) {
            dynamicSettings.save(file);
        }

        return dynamicSettings;
    }

    public RedisHandler getRedis() {
        return redis;
    }

    @SuppressWarnings("deprecation")
    private void register() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(this, new BukkitRunnable() {
            @Override
            public void run() {
                List<String> players = new ArrayList<String>();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    players.add(player.getName());
                }

                NetTask.withName("heartbeat")
                        .withArg("name", "" + dynamicSettings.getName())
                        .withArg("ip", Bukkit.getIp())
                        .withArg("port", Bukkit.getPort())
                        .withArg("players", players)
                        .withArg("vipOnly", vipOnly)
                        .send("heartbeat", redis);
            }
        }, 10 * 20, 10 * 20);
    }

    public static CRBukkit getInstance() {
        return instance;
    }

    public DynamicSettings getDynamicSettings() {
        return dynamicSettings;
    }

    public static boolean isVipOnly() {
        return vipOnly;
    }

    public static void setVipOnly(boolean vipOnly) {
        CRBukkit.vipOnly = vipOnly;
    }
}
